# Test Documentation

## Linting

Python files are linted with [pycodestyle](https://pypi.org/project/pycodestyle/)
and shell files with [shellcheck](https://github.com/koalaman/shellcheck).
On Mac OS, they can be installed with:

```python
pip install pycodestyle
brew install shellcheck
```

Both linters can be run with:

```sh
./test/test-code-style.sh
```

## Unit tests

These tests cover the Python scripts that wrap ZAP. You will need Python 3
installed on your system with a `python3` executable to run them.

To install the tests' dependencies:

```sh
pip install -r ./requirements-test.txt
```

To run all the tests:

```sh
./test/test-unit.sh
```

To run a specific file:

```sh
./test/test-unit.sh -p test_file.py # supports glob matching e.g. test_specific_*.py
```

## End to end tests

The end to end tests use Docker to run DAST against a test target application. Note
that the tests will not build a new DAST image for you. This must be done
yourself by the following the instructions in the [README](../README.md#build-image).

To avoid noise in the test output, each test outputs the ZAP logs to
`./test/end-to-end/output/[test-name].log`. These log files are published as artifacts
in the CI job.

### Prerequisites

These tests rely on [bash\_unit](https://github.com/pgrange/bash_unit), so
begin by installing it.

To install bash\_unit on Mac OS:

```sh
bash <(curl -s https://raw.githubusercontent.com/pgrange/bash_unit/master/install.sh)
mv ./bash_unit /usr/local/bin
```

### Running

Run all E2E tests with:

```sh
docker build -t dast . && ./test/test-e2e.sh
```

#### Baseline scan test

To build DAST and run a baseline scan against a [simple web site](../test/end-to-end/fixtures/basic-site/index.html),
execute the following from the project's root directory:

```sh
docker build -t dast . && bash_unit ./test/end-to-end/test-baseline.sh
```

#### Full scan test

To build DAST and run a full scan against WebGoat 8, execute the following
from the project's root directory:

```sh
docker build -t dast . && bash_unit ./test/end-to-end/test-full-scan.sh
```

##### WebGoat

For the full scan test, WebGoat is started with a [prepopulated database](../test/end-to-end/fixtures/webgoat-data.tar.gz) containing a user.
The username is `someone` and password is `P@ssw0rd`.
The test uses these credentials to perform an authenticated scan of WebGoat.

If you want to create a different user for testing, follow these steps:

1. Start the WebGoat docker image.
1. Register a new user in WebGoat. WebGoat will persist this user in its database located at `/home/webgoat/.webgoat-8.0.0.M21/`.
1. Copy the files in `/home/webgoat/.webgoat-8.0.0.M21/`.
1. When starting a new WebGoat instance, restore the files copied in the previous step to the same directory.
