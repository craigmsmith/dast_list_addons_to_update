#!/bin/sh

set -e

error() {
  echo "$*" >>/dev/stderr
  exit 1
}

PROJECT_DIR="$(dirname "$(realpath "$0")")/.."
CHANGELOG_FILE=$(realpath "$PROJECT_DIR/CHANGELOG.md")

if ! [ -f "$CHANGELOG_FILE" ]; then
  error "Unable to find $CHANGELOG_FILE."
fi

# find the first matching version, e.g. ## v1.6.0
VERSION_PATTERN='^## \(v[0-9]*\.[0-9]*\.[0-9]*\)$'
VERSION=$(sed -n "s/$VERSION_PATTERN/\\1/p" "$CHANGELOG_FILE" | sed -n '1,1p;1q')

if [ -z "$VERSION" ]; then
  error "Aborting, unable to determine the latest version in the changelog file. Expected line with version to have format like '## v1.3.6'."
fi

# find the first line that starts with ## (should also be the first matching version)
MOST_RECENT_VERSION=$(grep '^##.*$' "$CHANGELOG_FILE" | head -n 1 | sed 's/## //')

if [ "$MOST_RECENT_VERSION" != "$VERSION" ]; then
  error "The most recent version in the changelog $MOST_RECENT_VERSION does not conform to the expected format. Expected version line to have format like '## v1.3.6'."
fi

# extract the latest version description from the CHANGELOG
CHANGELOG_DESCRIPTION_START=$(sed -n "/$VERSION_PATTERN/=" "$CHANGELOG_FILE" | sed -n '1,1p;1q' | awk '{print $0 + 1}')
CHANGELOG_DESCRIPTION_END=$(sed -n "/$VERSION_PATTERN/=" "$CHANGELOG_FILE" | sed -n '2,2p;2q' | awk '{print $0 - 2}')
CHANGELOG_DESCRIPTION=$(sed -n "${CHANGELOG_DESCRIPTION_START},${CHANGELOG_DESCRIPTION_END}p;${CHANGELOG_DESCRIPTION_END}q" "$CHANGELOG_FILE")

# remove new lines so it can be added to a JSON document
CHANGELOG_DESCRIPTION=$(echo "$CHANGELOG_DESCRIPTION" | tr '\n' ' ' | sed 's/.$//')

RELEASE_DATA="{\"tag_name\":\"$VERSION\",\"description\":\"$CHANGELOG_DESCRIPTION\"}"
JSON_TYPE=$(echo "$RELEASE_DATA" | jq type | sed "s/\"//g")

if [ "$JSON_TYPE" != "object" ]; then
  error "Aborting, extracted release data type '$JSON_TYPE' is not a JSON object $RELEASE_DATA"
fi

TAG_NAME=$(echo "$RELEASE_DATA" | jq ".tag_name" | sed "s/\"//g")
DESCRIPTION=$(echo "$RELEASE_DATA" | jq ".description" | sed "s/\"//g")

if [ -z "$TAG_NAME" ]; then
  error "Aborting, unable to determine the tag name from the release data $RELEASE_DATA"
fi

if [ -z "$DESCRIPTION" ]; then
  error "Aborting, unable to determine the description from the release data $RELEASE_DATA"
fi

echo "$RELEASE_DATA"
