import json
import os

from dast_exception import DastException


class CustomHooks():

    WRK_DIR = '/zap/wrk/'

    def __init__(self, webdriver_factory, zaproxy, dast_report_formatter, system, logging, config):
        self.webdriver_factory = webdriver_factory
        self.opts = None
        self.json_report = None
        self.zaproxy = zaproxy
        self.dast_report_formatter = dast_report_formatter
        self.system = system
        self.logging = logging
        self.config = config

    # used by baseline and full scans
    def zap_access_target(self, zap, target):
        self.logging.debug('zap_access_target')

        self.zaproxy.new_session(zap, target)
        self.zaproxy.new_context()

        webdriver = self.webdriver_factory()

        try:
            if webdriver.is_authentication_required():
                webdriver.setup_webdriver(zap, target)
                webdriver.login(zap, target)
        except Exception as e:
            self.logging.error('Authentication Error')
            self.logging.exception(e)
        finally:
            webdriver.cleanup()

    # used by API scans
    def urls_imported(self, zap, urls):
        # use the first URL scanned as the target URL
        self.zaproxy.new_session(zap, urls[0])

    def cli_opts(self, opts):
        self.logging.info(f'Script params: {str(opts)}')
        self.opts = opts

        json_report = [x[1] for x in self.opts if x[0] == '-J']
        self.json_report = json_report[0] if json_report else None

    def zap_pre_shutdown(self, zap):
        self.logging.debug('zap_pre_shutdown')

        if not self.json_report:
            return

        scan, spider_scan_results = self.__spider_scan_results(zap)
        scanned_resources = self.zaproxy.scanned_resources()
        self.__write_reports(scan, spider_scan_results, scanned_resources)
        self.__log_urls(scanned_resources)

    def pre_exit(self, fail_count, warn_count, pass_count):
        # ZAP exits with an exit code based on pass/warn/fail counts.
        # We exit here so that we can exit with a 0 if the scan made it this far (i.e. ran successfully)
        self.system.exit(0)

    def __log_urls(self, scanned_resources):
        if scanned_resources.count() < 1:
            self.logging.info(
                f'{scanned_resources.count()} URLs were scanned.')
            return

        request_headers = scanned_resources.scanned_resources_as_string()
        self.logging.info(
            f'The following {scanned_resources.count()} URLs were scanned:\n{request_headers}')

    def __write_reports(self, scan, spider_scan_results, scanned_resources):
        with open(os.path.join(self.WRK_DIR, self.json_report), 'r+') as file:
            report = self.dast_report_formatter.format(
                json.load(file), scanned_resources, scan, spider_scan_results)
            file.seek(0)
            json.dump(report, file)
            file.truncate()

    def __spider_scan_results(self, zap):
        if self.config.is_api_scan:
            return {'progress': '100', 'state': 'FINISHED', 'id': '0'}, [{'urlsInScope': [{}]}]

        all_scans = zap.spider.scans

        if not all_scans:
            raise DastException('Error: no scans were performed. ' +
                                'Check log file zap.out to find out the reason.')

        # baseline scan and full scan should only perform one scan,
        # warn if for some reason multiple scans were performed
        if len(all_scans) > 1:
            self.logging.warning('More than one scan was executed. ' +
                                 'Reporting only results from first run.')

        scan = all_scans[0]
        spider_scan_results = zap.spider.full_results(int(scan['id']))
        return scan, spider_scan_results
