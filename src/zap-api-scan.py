#!/usr/bin/env python

# [DEPRECATED]
# Will be removed in the next major DAST release https://gitlab.com/gitlab-org/gitlab/issues/207066

# This script wraps zap-api-scan.py, which is coming with OWASP ZAP, for the purpose of supporting
# the syntax of GitLab DAST jobs as defined at
# https://docs.gitlab.com/ee/user/application_security/dast/index.html

from dependencies import APIScan

if __name__ == "__main__":
    APIScan.run()
