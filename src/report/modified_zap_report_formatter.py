from collections import OrderedDict
import itertools


class ModifiedZapReportFormatter:

    def format(self, zap_report, scanned_resources, spider_scan, spider_scan_results):
        formatted = OrderedDict()

        if zap_report is None or len(zap_report) == 0:
            raise RuntimeError('Unable to create DAST report as there is no ZAP report')

        if spider_scan is None or len(spider_scan) == 0:
            raise RuntimeError('Unable to create DAST report as there is no spider scan')

        if spider_scan_results is None or len(spider_scan_results) == 0:
            raise RuntimeError('Unable to create DAST report as there are no spider scan results')

        # Legacy DAST format
        formatted['@generated'] = zap_report.get('@generated', '')
        formatted['@version'] = zap_report.get('@version', '')
        formatted['site'] = self.__format_sites(zap_report.get('site', []))
        formatted['spider'] = spider = OrderedDict()
        spider['progress'] = spider_scan.get('progress', '')
        spider['result'] = OrderedDict()
        spider['result']['urlsInScope'] = self.__format_urls(self.__collect(spider_scan_results, 'urlsInScope'))
        spider['result']['urlsIoError'] = self.__format_urls(self.__collect(spider_scan_results, 'urlsIoError'))
        spider['result']['urlsOutOfScope'] = sorted(self.__collect(spider_scan_results, 'urlsOutOfScope'))
        spider['state'] = spider_scan.get('state', '')

        return formatted

    def __collect(self, values, property):
        properties = [value.get(property, []) for value in values]
        return list(itertools.chain(*properties))

    def __format_sites(self, sites):
        sorted_sites = sorted(sites, key=self.sort_by('@name'))
        return [self.__format_site(site) for site in sorted_sites]

    def __format_site(self, site):
        formatted = OrderedDict()
        formatted['@host'] = site.get('@host', '')
        formatted['@name'] = site.get('@name', '')
        formatted['@port'] = site.get('@port', '')
        formatted['@ssl'] = site.get('@ssl', '')
        formatted['alerts'] = self.__format_alerts(site.get('alerts', []))
        return formatted

    def __format_alerts(self, alerts):
        sorted_alerts = sorted(alerts, key=lambda a: (-int(a.get('riskcode', '1')), a.get('name', '')))
        return [self.__format_alert(alert) for alert in sorted_alerts]

    def __format_alert(self, alert):
        formatted = OrderedDict()
        formatted['alert'] = alert.get('alert', '')
        formatted['confidence'] = alert.get('confidence', '')
        formatted['count'] = alert.get('count', '')
        formatted['cweid'] = alert.get('cweid', '')
        formatted['desc'] = alert.get('desc', '')
        formatted['instances'] = self.__format_alert_instances(alert.get('instances', []))
        formatted['name'] = alert.get('name', '')
        formatted['otherinfo'] = alert.get('otherinfo', '')
        formatted['pluginid'] = alert.get('pluginid', '')
        formatted['reference'] = alert.get('reference', '')
        formatted['riskcode'] = alert.get('riskcode', '')
        formatted['riskdesc'] = alert.get('riskdesc', '')
        formatted['solution'] = alert.get('solution', '')
        formatted['sourceid'] = alert.get('sourceid', '')
        formatted['wascid'] = alert.get('wascid', '')

        if not formatted['pluginid']:
            raise RuntimeError("Unable to create DAST report as "
                               "there is no pluginid for alert '{}'".format(formatted['name']))

        return formatted

    def __format_alert_instances(self, instances):
        sorted_instances = sorted(instances, key=self.sort_by('uri', 'method', 'param'))
        return [self.__format_alert_instance(instance) for instance in sorted_instances]

    def __format_alert_instance(self, instance):
        formatted = OrderedDict()
        formatted['attack'] = instance.get('attack', '')
        formatted['evidence'] = instance.get('evidence', '')
        formatted['method'] = instance.get('method', '')
        formatted['param'] = instance.get('param', '')
        formatted['uri'] = instance.get('uri', '')
        return formatted

    def __format_urls(self, urls):
        sorted_urls = sorted(urls, key=self.sort_by('url', 'method'))
        return [self.__format_url(url) for url in sorted_urls]

    def __format_url(self, url):
        formatted = OrderedDict()
        formatted['method'] = url.get('method', '')
        formatted['processed'] = url.get('processed', '')
        formatted['reasonNotProcessed'] = url.get('reasonNotProcessed', '')
        formatted['statusCode'] = url.get('statusCode', '')
        formatted['statusReason'] = url.get('statusReason', '')
        formatted['url'] = url.get('url', '')

        return formatted

    def sort_by(self, *properties):
        return lambda value: ':'.join([value.get(prop, '') for prop in properties])
