import unittest
from unittest.mock import MagicMock
from target_website import TargetWebsite
from mock_config import ToConfig
import utilities


class TargetWebsiteTest(unittest.TestCase):

    # is_configured
    def test_is_configured_should_return_false_if_target_is_not_set(self):
        target_website = TargetWebsite(MagicMock(), ToConfig())
        self.assertEqual(target_website.is_configured(), False)

    def test_is_configured_should_return_true_if_target_is_set(self):
        target_website = TargetWebsite(MagicMock(), ToConfig(target="http://website"))
        self.assertEqual(target_website.is_configured(), True)

    # is_url
    def test_is_not_url_if_not_configured(self):
        target_website = TargetWebsite(MagicMock(), ToConfig())
        self.assertEqual(target_website.is_url(), False)

    def test_is_url_if_a_https_url(self):
        target_website = TargetWebsite(MagicMock(), ToConfig(target="https://website"))
        self.assertEqual(target_website.is_url(), True)

    def test_is_url_if_a_http_url(self):
        target_website = TargetWebsite(MagicMock(), ToConfig(target="http://website"))
        self.assertEqual(target_website.is_url(), True)

    def test_is_not_a_url_when_represents_a_file(self):
        target_website = TargetWebsite(MagicMock(), ToConfig(target="api_spec.json"))
        self.assertEqual(target_website.is_url(), False)

    # site_is_available
    def test_site_is_available_when_response_is_ok(self):
        with utilities.httpserver.new().respond(status=200).build() as server:
            target_website = TargetWebsite(MagicMock(), ToConfig(target=server.address()))
            site_check = target_website.check_site_is_available()
            self.assertEqual(site_check.is_available(), True)

    def test_site_is_available_when_response_is_redirect(self):
        with utilities.httpserver.new().respond(status=302).build() as server:
            target_website = TargetWebsite(MagicMock(), ToConfig(target=server.address()))
            site_check = target_website.check_site_is_available()
            self.assertEqual(site_check.is_available(), True)

    def test_site_is_unavailable_when_response_is_not_found(self):
        with utilities.httpserver.new().respond(status=404).build() as server:
            target_website = TargetWebsite(MagicMock(), ToConfig(target=server.address()))
            site_check = target_website.check_site_is_available(max_attempts=1)
            self.assertEqual(site_check.is_available(), False)

    def test_site_is_unavailable_when_cannot_connect_to_server(self):
        port = utilities.httpserver.get_free_port()
        target_website = TargetWebsite(MagicMock(), ToConfig(target=f"http://127.0.0.1:{port}/"))
        site_check = target_website.check_site_is_available(max_attempts=1)
        self.assertEqual(site_check.is_available(), False)

    def test_should_retry_when_at_first_you_dont_succeed(self):
        with utilities.httpserver.new().respond(status=404).respond(status=404).respond(status=200).build() as server:
            target_website = TargetWebsite(MagicMock(), ToConfig(target=server.address()))
            site_check = target_website.check_site_is_available(max_attempts=3, retry_delay=0)
            self.assertEqual(site_check.is_available(), True)

    def test_site_is_unavailable_when_server_responds_slowly(self):
        with utilities.httpserver.new().respond(delay=2, status=200).build() as server:
            target_website = TargetWebsite(MagicMock(), ToConfig(target=server.address()))
            site_check = target_website.check_site_is_available(max_attempts=1, request_timeout=1)
            self.assertEqual(site_check.is_available(), False)

    def test_site_is_available_when_using_invalid_https_certificate(self):
        self_signed_cert = 'test/unit/fixtures/certificates/self-signed'

        with utilities.httpserver.new().https(self_signed_cert).respond(status=200).build() as server:
            target_website = TargetWebsite(MagicMock(), ToConfig(target=server.address()))
            site_check = target_website.check_site_is_available(max_attempts=1)
            self.assertEqual(site_check.is_available(), True)
