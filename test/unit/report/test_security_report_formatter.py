import unittest
from unittest.mock import MagicMock
from src.report import SecurityReportFormatter
from scanned_resources import ScannedResources
import factories


class ToObject:
    def __init__(self, **key_values):
        self.__dict__.update(key_values)


def zap_report():
    return factories.zap_report.report(alerts=[
        {
            'count': '1',
            'riskdesc': 'Low (Medium)',
            'name': 'Web Browser XSS Protection Not Enabled',
            'reference': '<p>http://blogs.msdn.com/a</p><p>http://blogs.msdn.com/b</p>',
            'otherinfo': '<p>The X-XSS-Protection HTTP response header allows the web server ...</p>',
            'sourceid': '3',
            'confidence': '2',
            'alert': 'Web Browser XSS Protection Not Enabled',
            'instances': [
                {
                    'attack': 'http://nginx/attack',
                    'evidence': '<form action="/myform" method="POST">',
                    'uri': 'http://nginx/',
                    'param': 'X-XSS-Protection',
                    'method': 'POST'
                }
            ],
            'pluginid': '10016',
            'riskcode': '1',
            'wascid': '14',
            'solution': '<p>Ensure that the web browsers XSS filter is enabled...</p>',
            'cweid': '933',
            'desc': '<p>Web Browser XSS Protection is not enabled, or is disabled...</p>'
        },
        {
            'count': '2',
            'riskdesc': 'Medium (Medium)',
            'name': 'X-Frame-Options Header Not Set',
            'reference': '<p>http://blogs.msdn.com/b/ieinternals/archive/2010/03/30/...</p>',
            'sourceid': '3',
            'confidence': '2',
            'alert': 'X-Frame-Options Header Not Set',
            'instances': [
                {
                    'uri': 'http://nginx/b.location',
                    'param': 'X-Frame-Options',
                    'method': 'GET'
                },
                {
                    'uri': 'http://nginx/a.location',
                    'param': 'X-Frame-Options',
                    'method': 'GET'
                }
            ],
            'pluginid': '10020',
            'riskcode': '2',
            'wascid': '15',
            'solution': '<p>Most modern Web browsers support the X-Frame-Options HTTP header...</p>',
            'cweid': '16',
            'desc': '<p>X-Frame-Options header is not included in the HTTP response...</p>'
        }
    ])


class SecurityReportFormatterTest(unittest.TestCase):

    def setUp(self):
        self.wasc = ToObject(reference_url_for_id=MagicMock(return_value='http://projects.webappsec.org/reference'))
        self.messages = [
            {'url': 'http://nginx/', 'method': 'GET'},
            {'url': 'http://nginx/a.txt', 'method': 'POST'},
            {'url': 'http://nginx/a.txt', 'method': 'GET'},
            {'url': 'http://nginx/robots.txt', 'method': 'GET'},
            {'url': 'http://nginx/myform', 'method': 'POST'}
        ]
        self.uuid = MagicMock()
        self.uuid.uuid4 = MagicMock(return_value='1234')
        self.scanned_resources = ScannedResources(self.messages)
        self.report = SecurityReportFormatter(self.wasc, self.uuid).format(zap_report(), self.scanned_resources)

    def test_should_contain_a_version(self):
        self.assertTrue(self.report['version'])

    def test_when_zap_does_not_return_a_url_it_should_not_error(self):
        scanned_resources = ScannedResources([{'url': '', 'method': 'GET'}])
        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap_report(), scanned_resources)
        scanned_resource = report['scan']['scanned_resources'][0]
        self.assertEqual(scanned_resource['url'], '')

    def test_scanned_resources_include_all_properties(self):
        self.assertEqual(len(self.report['scan']['scanned_resources']), 5)

        scanned_resource = self.report['scan']['scanned_resources'][4]
        self.assertEqual(scanned_resource['type'], 'url')
        self.assertEqual(scanned_resource['url'], 'http://nginx/robots.txt')
        self.assertEqual(scanned_resource['method'], 'GET')

    def test_scanned_resources_should_be_sorted_by_type_url_and_method(self):
        resource_0 = self.report['scan']['scanned_resources'][0]
        self.assertEqual(resource_0['url'], 'http://nginx/')
        self.assertEqual(resource_0['method'], 'GET')

        resource_1 = self.report['scan']['scanned_resources'][1]
        self.assertEqual(resource_1['url'], 'http://nginx/a.txt')
        self.assertEqual(resource_1['method'], 'GET')

        resource_2 = self.report['scan']['scanned_resources'][2]
        self.assertEqual(resource_2['url'], 'http://nginx/a.txt')
        self.assertEqual(resource_2['method'], 'POST')

        resource_3 = self.report['scan']['scanned_resources'][3]
        self.assertEqual(resource_3['url'], 'http://nginx/myform')
        self.assertEqual(resource_3['method'], 'POST')

        resource_4 = self.report['scan']['scanned_resources'][4]
        self.assertEqual(resource_4['url'], 'http://nginx/robots.txt')
        self.assertEqual(resource_4['method'], 'GET')

    def test_vulnerabilities_include_all_properties(self):
        self.assertEqual(len(self.report['vulnerabilities']), 3)

        vulnerability = self.report['vulnerabilities'][2]
        self.assertEqual(vulnerability['category'], 'dast')
        self.assertEqual(vulnerability['confidence'], 'Medium')
        self.assertEqual(vulnerability['cve'], '10016')
        self.assertEqual(vulnerability['id'], '1234')
        self.assertEqual(vulnerability['description'], 'Web Browser XSS Protection is not enabled, or is disabled...')
        self.assertEqual(len(vulnerability['identifiers']), 3)
        self.assertEqual(vulnerability['identifiers'][0]['name'], 'Web Browser XSS Protection Not Enabled')
        self.assertEqual(vulnerability['identifiers'][0]['type'], 'ZAProxy_PluginId')
        self.assertIn('https://github.com/zaproxy/zaproxy/', vulnerability['identifiers'][0]['url'])
        self.assertEqual(vulnerability['identifiers'][0]['value'], '10016')
        self.assertEqual(vulnerability['identifiers'][1]['name'], 'CWE-933')
        self.assertEqual(vulnerability['identifiers'][1]['type'], 'CWE')
        self.assertEqual(vulnerability['identifiers'][1]['url'], 'https://cwe.mitre.org/data/definitions/933.html')
        self.assertEqual(vulnerability['identifiers'][1]['value'], '933')
        self.assertEqual(vulnerability['identifiers'][2]['name'], 'WASC-14')
        self.assertEqual(vulnerability['identifiers'][2]['type'], 'WASC')
        self.assertEqual(vulnerability['identifiers'][2]['url'], 'http://projects.webappsec.org/reference')
        self.assertEqual(vulnerability['identifiers'][2]['value'], '14')
        self.assertEqual(len(vulnerability['links']), 2)
        self.assertEqual(vulnerability['links'][0]['url'], 'http://blogs.msdn.com/a')
        self.assertEqual(vulnerability['links'][1]['url'], 'http://blogs.msdn.com/b')
        self.assertEqual(vulnerability['location']['param'], 'X-XSS-Protection')
        self.assertEqual(vulnerability['location']['method'], 'POST')
        self.assertEqual(vulnerability['location']['hostname'], 'http://nginx')
        self.assertEqual(vulnerability['location']['path'], '/')
        self.assertEqual(vulnerability['message'], 'Web Browser XSS Protection Not Enabled')
        self.assertEqual(vulnerability['scanner']['id'], 'zaproxy')
        self.assertEqual(vulnerability['scanner']['name'], 'ZAProxy')
        self.assertEqual(vulnerability['severity'], 'Low')
        self.assertEqual(vulnerability['solution'], 'Ensure that the web browsers XSS filter is enabled...')

    def test_vulnerabilities_are_ordered_by_name_and_risk(self):
        self.assertEqual(len(self.report['vulnerabilities']), 3)

        self.assertEqual(self.report['vulnerabilities'][0]['message'], 'X-Frame-Options Header Not Set')
        self.assertIn('/a.location', self.report['vulnerabilities'][0]['location']['path'])
        self.assertEqual(self.report['vulnerabilities'][1]['message'], 'X-Frame-Options Header Not Set')
        self.assertIn('/b.location', self.report['vulnerabilities'][1]['location']['path'])
        self.assertEqual(self.report['vulnerabilities'][2]['message'], 'Web Browser XSS Protection Not Enabled')
        self.assertIn('/', self.report['vulnerabilities'][2]['location']['path'])

    def test_vulnerability_severity_is_mapped_to_human_readable_form(self):
        def build_report_with_zap_riskcode(riskcode):
            zap = factories.zap_report.report_with_alert(alert_values={'riskcode': riskcode})
            return SecurityReportFormatter(self.wasc, self.uuid).format(zap, self.scanned_resources)

        report = build_report_with_zap_riskcode('0')
        self.assertEqual(report['vulnerabilities'][0]['severity'], 'Info')

        report = build_report_with_zap_riskcode('1')
        self.assertEqual(report['vulnerabilities'][0]['severity'], 'Low')

        report = build_report_with_zap_riskcode('2')
        self.assertEqual(report['vulnerabilities'][0]['severity'], 'Medium')

        report = build_report_with_zap_riskcode('3')
        self.assertEqual(report['vulnerabilities'][0]['severity'], 'High')

        report = build_report_with_zap_riskcode('666')
        self.assertEqual(report['vulnerabilities'][0]['severity'], 'Unknown')

    def test_vulnerability_confidence_is_mapped_to_human_readable_form(self):
        def build_report_with_zap_confidence(confidence):
            zap = factories.zap_report.report_with_alert(alert_values={'confidence': confidence})
            return SecurityReportFormatter(self.wasc, self.uuid).format(zap, self.scanned_resources)

        report = build_report_with_zap_confidence('0')
        self.assertEqual(report['vulnerabilities'][0]['confidence'], 'Ignore')

        report = build_report_with_zap_confidence('1')
        self.assertEqual(report['vulnerabilities'][0]['confidence'], 'Low')

        report = build_report_with_zap_confidence('2')
        self.assertEqual(report['vulnerabilities'][0]['confidence'], 'Medium')

        report = build_report_with_zap_confidence('3')
        self.assertEqual(report['vulnerabilities'][0]['confidence'], 'High')

        report = build_report_with_zap_confidence('4')
        self.assertEqual(report['vulnerabilities'][0]['confidence'], 'Confirmed')

        report = build_report_with_zap_confidence('666')
        self.assertEqual(report['vulnerabilities'][0]['confidence'], 'Unknown')

    def test_vulnerability_location_contains_uri_when_contains_query_string(self):
        uri = 'https://fred:flintstone@domain.com/some/path?search=foo#fragment'
        instance = factories.zap_report.alert_instance(uri=uri)
        zap = factories.zap_report.report_with_alert(alert_values={'instances': [instance], 'otherinfo': '<p></p>'})

        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, self.scanned_resources)

        self.assertEqual(report['vulnerabilities'][0]['location']['hostname'], 'https://fred:flintstone@domain.com')
        self.assertEqual(report['vulnerabilities'][0]['location']['path'], '/some/path?search=foo#fragment')

    def test_vulnerability_location_contains_uri_when_has_no_path(self):
        instance = factories.zap_report.alert_instance(uri='https://website.com')
        zap = factories.zap_report.report_with_alert(alert_values={'instances': [instance], 'otherinfo': '<p></p>'})

        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, self.scanned_resources)

        self.assertEqual(report['vulnerabilities'][0]['location']['hostname'], 'https://website.com')
        self.assertEqual(report['vulnerabilities'][0]['location']['path'], '')

    def test_vulnerability_location_contains_uri_when_ends_in_slash(self):
        instance = factories.zap_report.alert_instance(uri='https://website.com/')
        zap = factories.zap_report.report_with_alert(alert_values={'instances': [instance], 'otherinfo': '<p></p>'})

        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, self.scanned_resources)

        self.assertEqual(report['vulnerabilities'][0]['location']['hostname'], 'https://website.com')
        self.assertEqual(report['vulnerabilities'][0]['location']['path'], '/')

    def test_should_not_have_cwe_identifier_when_not_present(self):
        zap = factories.zap_report.report_with_alert(alert_values={'cwe_id': ''})
        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, self.scanned_resources)

        types = [identifier['type'] for identifier in report['vulnerabilities'][0]['identifiers']]
        self.assertFalse('CWE' in types)

    def test_should_not_have_wasc_identifier_when_not_present(self):
        zap = factories.zap_report.report_with_alert(alert_values={'wascid': ''})
        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, self.scanned_resources)

        types = [identifier['type'] for identifier in report['vulnerabilities'][0]['identifiers']]
        self.assertFalse('WASC' in types)

    def test_should_barf_when_vulnerability_has_no_pluginid(self):
        zap = factories.zap_report.report_with_alert(alert_values={'pluginid': ''})

        try:
            SecurityReportFormatter(self.wasc, self.uuid).format(zap, self.scanned_resources)
            self.assertTrue(False)
        except RuntimeError as e:
            self.assertEqual(str(e), "Unable to create DAST report as there is no "
                                     "pluginid for alert 'Web Browser XSS Protection Not Enabled'")

    def test_should_add_evidence_and_other_info(self):
        instance = factories.zap_report.alert_instance(evidence='  378282246310005  ')
        other_info = '<p>Credit Card Type detected: American Express   </p>'
        zap = factories.zap_report.report_with_alert(alert_values={'instances': [instance], 'otherinfo': other_info})

        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, self.scanned_resources)

        evidence = report['vulnerabilities'][-1]['evidence']
        self.assertEqual(evidence['summary'], '378282246310005; '
                                              'Credit Card Type detected: American Express')

    def test_should_exclude_other_info_from_evidence_when_not_present(self):
        instance = factories.zap_report.alert_instance(evidence='error')
        zap = factories.zap_report.report_with_alert(alert_values={'instances': [instance], 'otherinfo': '<p></p>'})

        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, self.scanned_resources)
        self.assertEqual(report['vulnerabilities'][-1]['evidence']['summary'], 'error')

    def test_evidence_should_be_empty_when_has_no_value(self):
        instance = factories.zap_report.alert_instance(evidence='')
        zap = factories.zap_report.report_with_alert(alert_values={'instances': [instance], 'otherinfo': ''})

        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, self.scanned_resources)
        self.assertEqual(report['vulnerabilities'][0]['evidence']['summary'], '')

    def test_evidence_should_be_empty_when_alert_and_instance_is_empty(self):
        zap = factories.zap_report.report(alerts=[{
            'pluginid': '10001',
            'instances': [{}]
        }])

        report = SecurityReportFormatter(self.wasc, self.uuid).format(zap, self.scanned_resources)
        self.assertEqual(report['vulnerabilities'][0]['evidence']['summary'], '')
