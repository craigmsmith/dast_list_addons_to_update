import unittest
from src.report import ModifiedZapReportFormatter


def zap_report():
    return {
        '@generated': 'Tue, 22 Oct 2019 01:01:55',
        '@version': 'D-2019-09-23',
        'site': [
            {
                '@port': '80',
                '@host': 'nginx',
                '@name': 'http://nginx',
                'alerts': [
                    {
                        'count': '1',
                        'riskdesc': 'Low (Medium)',
                        'name': 'Web Browser XSS Protection Not Enabled',
                        'reference': '<p>http://blogs.msdn.com/a</p><p>http://blogs.msdn.com/b</p>',
                        'otherinfo': '<p>The X-XSS-Protection HTTP response header allows the web server ...</p>',
                        'sourceid': '3',
                        'confidence': '2',
                        'alert': 'Web Browser XSS Protection Not Enabled',
                        'instances': [
                            {
                                'attack': 'http://nginx/attack',
                                'evidence': '<form action="/myform" method="POST">',
                                'uri': 'http://nginx/',
                                'param': 'X-XSS-Protection',
                                'method': 'POST'
                            }
                        ],
                        'pluginid': '10016',
                        'riskcode': '1',
                        'wascid': '14',
                        'solution': '<p>Ensure that the web browsers XSS filter is enabled...</p>',
                        'cweid': '933',
                        'desc': '<p>Web Browser XSS Protection is not enabled, or is disabled...</p>'
                    },
                    {
                        'count': '2',
                        'riskdesc': 'Medium (Medium)',
                        'name': 'X-Frame-Options Header Not Set',
                        'reference': '<p>http://blogs.msdn.com/b/ieinternals/archive/2010/03/30/...</p>',
                        'sourceid': '3',
                        'confidence': '2',
                        'alert': 'X-Frame-Options Header Not Set',
                        'instances': [
                            {
                                'uri': 'http://nginx/b.location',
                                'param': 'X-Frame-Options',
                                'method': 'GET'
                            },
                            {
                                'uri': 'http://nginx/a.location',
                                'param': 'X-Frame-Options',
                                'method': 'GET'
                            }
                        ],
                        'pluginid': '10020',
                        'riskcode': '2',
                        'wascid': '15',
                        'solution': '<p>Most modern Web browsers support the X-Frame-Options HTTP header...</p>',
                        'cweid': '16',
                        'desc': '<p>X-Frame-Options header is not included in the HTTP response...</p>'
                    }
                ],
                '@ssl': 'false'
            }
        ]
    }


def spider_scan():
    return {
        'progress': '100',
        'state': 'FINISHED',
        'id': '0'
    }


def spider_scan_result():
    return [
        {
            'urlsInScope': [
                {
                    'url': 'http://f.url',
                    'statusReason': 'OK',
                    'reasonNotProcessed': '',
                    'processed': 'true',
                    'method': 'GET',
                    'statusCode': '200',
                    'messageId': '1'
                },
                {
                    'url': 'http://e.url',
                    'statusReason': 'OK',
                    'reasonNotProcessed': '',
                    'processed': 'true',
                    'method': 'GET',
                    'statusCode': '201',
                    'messageId': '2'
                }
            ]
        },
        {
            'urlsOutOfScope': ['http://d.url', 'http://c.url']
        },
        {
            'urlsIoError': [
                {
                    'url': 'http://b.url',
                    'statusReason': 'Internal Server Error',
                    'reasonNotProcessed': '',
                    'processed': 'false',
                    'method': 'GET',
                    'statusCode': '500'
                },
                {
                    'url': 'http://a.url',
                    'statusReason': 'Bad Gateway',
                    'reasonNotProcessed': '',
                    'processed': 'false',
                    'method': 'GET',
                    'statusCode': '502'
                }
            ]
        }
    ]


class ModifiedZapReportFormatterTest(unittest.TestCase):

    def setUp(self):
        self.report = ModifiedZapReportFormatter().format(zap_report(), '', spider_scan(), spider_scan_result())

    def test_should_remove_message_ids(self):
        self.assertEqual(len(self.report['spider']['result']['urlsInScope']), 2)
        self.assertFalse('messageId' in self.report['spider']['result']['urlsInScope'][0])
        self.assertFalse('messageId' in self.report['spider']['result']['urlsInScope'][1])

    def test_should_remove_scan_id(self):
        self.assertFalse('scanId' in self.report['spider'])

    def test_urls_should_be_ordered_by_url_and_method(self):
        self.assertEqual(len(self.report['spider']['result']['urlsInScope']), 2)
        self.assertEqual(self.report['spider']['result']['urlsInScope'][0]['url'], 'http://e.url')
        self.assertEqual(self.report['spider']['result']['urlsInScope'][1]['url'], 'http://f.url')

        self.assertEqual(len(self.report['spider']['result']['urlsOutOfScope']), 2)
        self.assertEqual(self.report['spider']['result']['urlsOutOfScope'][0], 'http://c.url')
        self.assertEqual(self.report['spider']['result']['urlsOutOfScope'][1], 'http://d.url')

        self.assertEqual(len(self.report['spider']['result']['urlsIoError']), 2)
        self.assertEqual(self.report['spider']['result']['urlsIoError'][0]['url'], 'http://a.url')
        self.assertEqual(self.report['spider']['result']['urlsIoError'][1]['url'], 'http://b.url')

    def test_url_keys_should_be_ordered_alphabetically(self):
        keys = list(self.report['spider']['result'].keys())
        self.assertEqual(keys[0], 'urlsInScope')
        self.assertEqual(keys[1], 'urlsIoError')
        self.assertEqual(keys[2], 'urlsOutOfScope')

        first_in_scope_url = list(self.report['spider']['result']['urlsInScope'][0].keys())
        self.assertEqual(first_in_scope_url[0], 'method')
        self.assertEqual(first_in_scope_url[1], 'processed')
        self.assertEqual(first_in_scope_url[2], 'reasonNotProcessed')
        self.assertEqual(first_in_scope_url[3], 'statusCode')
        self.assertEqual(first_in_scope_url[4], 'statusReason')
        self.assertEqual(first_in_scope_url[5], 'url')

    def test_should_collect_all_scanned_result(self):
        scan_results = [{'urlsOutOfScope': ['http://url.2']},
                        {'urlsOutOfScope': ['http://url.1']}]

        report = ModifiedZapReportFormatter().format(zap_report(), '', spider_scan(), scan_results)

        self.assertEqual(len(report['spider']['result']['urlsInScope']), 0)
        self.assertEqual(len(report['spider']['result']['urlsOutOfScope']), 2)
        self.assertEqual(report['spider']['result']['urlsOutOfScope'][0], 'http://url.1')
        self.assertEqual(report['spider']['result']['urlsOutOfScope'][1], 'http://url.2')
        self.assertEqual(len(report['spider']['result']['urlsIoError']), 0)

    def test_should_include_spider_progress(self):
        self.assertEqual(self.report['spider']['state'], 'FINISHED')
        self.assertEqual(self.report['spider']['progress'], '100')

    def test_should_include_timestamp_and_version(self):
        self.assertEqual(self.report['@generated'], 'Tue, 22 Oct 2019 01:01:55')
        self.assertEqual(self.report['@version'], 'D-2019-09-23')

    def test_should_include_site_metadata(self):
        self.assertEqual(len(self.report['site']), 1)
        self.assertEqual(self.report['site'][0]['@host'], 'nginx')
        self.assertEqual(self.report['site'][0]['@name'], 'http://nginx')
        self.assertEqual(self.report['site'][0]['@port'], '80')
        self.assertEqual(self.report['site'][0]['@ssl'], 'false')

    def test_site_alerts_include_all_properties(self):
        self.assertEqual(len(self.report['site']), 1)
        self.assertEqual(len(self.report['site'][0]['alerts']), 2)

        alert = self.report['site'][0]['alerts'][1]
        self.assertEqual(alert['alert'], 'Web Browser XSS Protection Not Enabled')
        self.assertEqual(alert['confidence'], '2')
        self.assertEqual(alert['count'], '1')
        self.assertEqual(alert['cweid'], '933')
        self.assertEqual(alert['desc'], '<p>Web Browser XSS Protection is not enabled, or is disabled...</p>')
        self.assertEqual(alert['name'], 'Web Browser XSS Protection Not Enabled')
        self.assertEqual(alert['otherinfo'],
                         '<p>The X-XSS-Protection HTTP response header allows the web server ...</p>')
        self.assertEqual(alert['pluginid'], '10016')
        self.assertEqual(alert['riskcode'], '1')
        self.assertEqual(alert['riskdesc'], 'Low (Medium)')
        self.assertEqual(alert['reference'], '<p>http://blogs.msdn.com/a</p><p>http://blogs.msdn.com/b</p>')
        self.assertEqual(alert['solution'], '<p>Ensure that the web browsers XSS filter is enabled...</p>')
        self.assertEqual(alert['sourceid'], '3')
        self.assertEqual(alert['wascid'], '14')

    def test_site_alerts_are_ordered_by_risk(self):
        self.assertEqual(len(self.report['site']), 1)
        self.assertEqual(len(self.report['site'][0]['alerts']), 2)
        self.assertEqual(self.report['site'][0]['alerts'][0]['name'], 'X-Frame-Options Header Not Set')
        self.assertEqual(self.report['site'][0]['alerts'][1]['name'], 'Web Browser XSS Protection Not Enabled')

    def test_site_alerts_include_instances(self):
        self.assertEqual(len(self.report['site']), 1)
        self.assertEqual(len(self.report['site'][0]['alerts']), 2)

        alert = self.report['site'][0]['alerts'][1]
        self.assertEqual(len(alert['instances']), 1)
        self.assertEqual(alert['instances'][0]['attack'], 'http://nginx/attack')
        self.assertEqual(alert['instances'][0]['evidence'], '<form action="/myform" method="POST">')
        self.assertEqual(alert['instances'][0]['method'], 'POST')
        self.assertEqual(alert['instances'][0]['param'], 'X-XSS-Protection')
        self.assertEqual(alert['instances'][0]['uri'], 'http://nginx/')

    def test_site_alerts_instances_are_sorted_by_uri(self):
        self.assertEqual(len(self.report['site']), 1)
        self.assertEqual(len(self.report['site'][0]['alerts']), 2)

        alert = self.report['site'][0]['alerts'][0]
        self.assertEqual(len(alert['instances']), 2)
        self.assertEqual(alert['instances'][0]['uri'], 'http://nginx/a.location')
        self.assertEqual(alert['instances'][1]['uri'], 'http://nginx/b.location')

    def test_should_barf_when_vulnerability_has_no_pluginid(self):
        zap_rpt = zap_report()
        zap_rpt['site'][0]['alerts'][0]['pluginid'] = ''

        try:
            ModifiedZapReportFormatter().format(zap_rpt, '', spider_scan(), spider_scan_result())
            self.assertTrue(False)
        except RuntimeError as e:
            self.assertEqual(str(e), "Unable to create DAST report as there is no "
                                     "pluginid for alert 'Web Browser XSS Protection Not Enabled'")
