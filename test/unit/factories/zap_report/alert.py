from .alert_instance import alert_instance as alert_instance_factory


def alert(instances=[alert_instance_factory()],
          otherinfo='<p>The X-XSS-Protection HTTP response header allows the web server ...</p>',
          pluginid='10016',
          wascid='14',
          cwe_id='933',
          confidence='2',
          riskcode='1'):
    return {
        'count': str(len(instances)),
        'riskdesc': 'Low (Medium)',
        'name': 'Web Browser XSS Protection Not Enabled',
        'reference': '<p>http://blogs.msdn.com/a</p><p>http://blogs.msdn.com/b</p>',
        'otherinfo': otherinfo,
        'sourceid': '3',
        'confidence': confidence,
        'alert': 'Web Browser XSS Protection Not Enabled',
        'instances': instances,
        'pluginid': pluginid,
        'riskcode': riskcode,
        'wascid': wascid,
        'solution': '<p>Ensure that the web browsers XSS filter is enabled...</p>',
        'cweid': cwe_id,
        'desc': '<p>Web Browser XSS Protection is not enabled, or is disabled...</p>'
    }


def alert_with_instance(instance_values={}, **kwargs):
    return alert(instances=[alert_instance_factory(**instance_values)], **kwargs)
