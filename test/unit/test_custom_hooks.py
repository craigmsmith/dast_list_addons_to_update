import os
import unittest
from collections import namedtuple
from unittest.mock import MagicMock, mock_open, patch

from custom_hooks import CustomHooks
from dast_exception import DastException
from mock_config import ToConfig
from scanned_resources import ScannedResources
from zap_webdriver import ZapWebdriver


class CustomHooksTest(unittest.TestCase):

    def webdriver(self):
        return ZapWebdriver()

    def setUp(self):
        self.scan_data = {'id': 0, 'progress': '100', 'state': 'FINISHED'}
        self.full_result_data = [{'urlsInScope': []},
                                 {'urlsOutOfScope': []},
                                 {'urlsIoError': []}]

        self.zap = MagicMock()
        self.zap.spider.scans = [self.scan_data]
        self.zap.spider.full_results = MagicMock(return_value=self.full_result_data)

        self.logger = MagicMock()
        self.zaproxy = MagicMock()
        self.config = ToConfig(is_api_scan=False)
        self.dast_report = {'problems': 'many'}
        report_formatter = namedtuple('rpt_formatter', 'format')(format=MagicMock(return_value=self.dast_report))
        self.hooks = CustomHooks(self.webdriver, self.zaproxy, report_formatter, MagicMock(), self.logger, self.config)
        self.hooks.json_report = 'foo.json'

    def test_cli_opts_with_report(self):
        acutal_name = 'foobar'
        opts = [('-J', acutal_name), ('-a', 'aaa'), ('-b', 'bbb')]
        self.hooks.cli_opts(opts)
        self.assertEqual(self.hooks.json_report, acutal_name)

    def test_cli_opts_without_report(self):
        opts = [('-a', 'aaa'), ('-b', 'bbb')]
        self.hooks.cli_opts(opts)
        self.assertIsNone(self.hooks.json_report)

    def test_zap_pre_shutdown_without_report(self):
        self.hooks.json_report = None
        self.hooks.zap_pre_shutdown(self.zap)  # should not raise

    def test_zap_pre_shutdown_no_scans(self):
        self.zap.spider.scans = []
        self.assertRaises(DastException, self.hooks.zap_pre_shutdown, self.zap)

    def test_zap_pre_shutdown_add_scanned_urls_to_report(self):
        self.zaproxy.scanned_resources = MagicMock(return_value=ScannedResources([
            {'url': 'http://nginx/', 'method': 'GET'},
            {'url': 'http://nginx/robots.txt', 'method': 'GET'}
        ]))

        with patch('builtins.open', new_callable=mock_open()) as fopen:
            with patch('json.load', return_value={'foo': 'bar'}) as jsonl:
                with patch('json.dump') as jsond:
                    self.hooks.zap_pre_shutdown(self.zap)
                    expected_path = os.path.join(self.hooks.WRK_DIR, self.hooks.json_report)
                    fopen.assert_called_with(expected_path, 'r+')
                    jsonl.assert_called_with(fopen().__enter__())
                    jsond.assert_called_with({'problems': 'many'}, fopen().__enter__())

    def test_zap_pre_shutdown_log_scanned_urls(self):
        self.zaproxy.scanned_resources = MagicMock(return_value=ScannedResources([
            {'url': 'http://nginx/', 'method': 'GET'},
            {'url': 'http://nginx/robots.txt', 'method': 'GET'}
        ]))

        with patch('builtins.open', new_callable=mock_open()) as fopen:
            with patch('json.load', return_value={'foo': 'bar'}) as jsonl:
                with patch('json.dump') as jsond:
                    expected_log = """The following 2 URLs were scanned:
GET http://nginx/
GET http://nginx/robots.txt"""

                    self.hooks.zap_pre_shutdown(self.zap)
                    self.logger.info.assert_called_once_with(expected_log)

    def test_zap_pre_shutdown_log_scanner_urls_when_no_urls_were_scanned(self):
        self.zaproxy.scanned_resources = MagicMock(return_value=ScannedResources([]))
        with patch('builtins.open', new_callable=mock_open()) as fopen:
            with patch('json.load', return_value={'foo': 'bar'}) as jsonl:
                with patch('json.dump') as jsond:
                    self.hooks.zap_pre_shutdown(self.zap)
                    self.logger.info.assert_called_once_with('0 URLs were scanned.')
