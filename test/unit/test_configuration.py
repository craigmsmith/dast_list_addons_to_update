import unittest
from argparse import ArgumentTypeError
from configuration import Configuration
import utilities


def merge(a, b):
    result = a.copy()
    result.update(b)
    return result


class ConfigurationTest(unittest.TestCase):
    DEFAULT_ENV = {'DAST_WEBSITE': 'http://website'}

    def test_can_set_api_schema_as_target(self):
        config = Configuration.load(['-t', 'spec.yml'], {})
        self.assertEqual(config.target, 'spec.yml')

    def test_can_set_target_using_option(self):
        config = Configuration.load(['-t', 'http://website'], {})
        self.assertEqual(config.target, 'http://website')

    def test_can_set_target_using_environment(self):
        config = Configuration.load([], {'DAST_WEBSITE': 'http://website'})
        self.assertEqual(config.target, 'http://website')

    def test_option_value_overrides_environment(self):
        config = Configuration.load(['-t', 'http://website'], {'DAST_WEBSITE': 'http://another.website'})
        self.assertEqual(config.target, 'http://website')

    def test_can_set_api_specification_using_option(self):
        config = Configuration.load(['--api-specification', 'http://website/api.spec'], {})
        self.assertEqual(config.api_specification, 'http://website/api.spec')
        self.assertTrue(config.is_api_scan)

    def test_can_set_api_specification_using_environment_alias(self):
        config = Configuration.load([], {'DAST_API_SPECIFICATION': 'http://website/api.spec'})
        self.assertEqual(config.api_specification, 'http://website/api.spec')
        self.assertTrue(config.is_api_scan)

    def test_is_not_api_scan_when_api_format_not_specified(self):
        config = Configuration.load(['-t', 'http://website'], {})
        self.assertFalse(config.is_api_scan)

    def test_domain_override_can_be_set_using_option(self):
        config = Configuration.load(['-O', 'my.host.com'], {})
        self.assertEqual(config.zap_api_host_override, 'my.host.com')

    def test_domain_override_can_be_set_using_environment(self):
        config = Configuration.load([], merge({'DAST_API_HOST_OVERRIDE': 'my.host.com'}, self.DEFAULT_ENV))
        self.assertEqual(config.zap_api_host_override, 'my.host.com')

    def test_can_not_set_invalid_url_as_auth_url(self):
        with self.assertRaises(SystemExit) as system_exit, utilities.capture_output() as output:
            Configuration.load(['--auth-url', 'loremipsum'], {})

        self.assertEqual(system_exit.exception.code, 2)
        self.assertIn('argument --auth-url: loremipsum is not a valid URL', output.stderr.getvalue())

    def test_can_set_auth_url_using_option(self):
        config = Configuration.load(['--auth-url', 'http://website'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_url, 'http://website')

    def test_can_set_auth_url_using_environment(self):
        config = Configuration.load([], merge({'DAST_AUTH_URL': 'http://website'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_url, 'http://website')

    def test_can_set_auth_url_using_legacy_environment(self):
        config = Configuration.load([], merge({'AUTH_URL': 'http://website'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_url, 'http://website')

    def test_can_set_auth_username_using_option(self):
        config = Configuration.load(['--auth-username', 'username'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_username, 'username')

    def test_can_set_auth_username_using_environment(self):
        config = Configuration.load([], merge({'DAST_USERNAME': 'username'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_username, 'username')

    def test_can_set_auth_username_using_legacy_environment(self):
        config = Configuration.load([], merge({'AUTH_USERNAME': 'username'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_username, 'username')

    def test_can_set_auth_password_using_option(self):
        config = Configuration.load(['--auth-password', 'password'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_password, 'password')

    def test_can_set_auth_password_using_environment(self):
        config = Configuration.load([], merge({'DAST_PASSWORD': 'password'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_password, 'password')

    def test_can_set_auth_password_using_legacy_environment(self):
        config = Configuration.load([], merge({'AUTH_PASSWORD': 'password'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_password, 'password')

    def test_can_set_username_field_using_option(self):
        config = Configuration.load(['--auth-username-field', 'username'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_username_field, 'username')

    def test_can_set_username_field_using_environment(self):
        config = Configuration.load([], merge({'DAST_USERNAME_FIELD': 'username'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_username_field, 'username')

    def test_can_set_username_field_using_legacy_environment(self):
        config = Configuration.load([], merge({'AUTH_USERNAME_FIELD': 'username'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_username_field, 'username')

    def test_can_set_password_field_using_option(self):
        config = Configuration.load(['--auth-password-field', 'password'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_password_field, 'password')

    def test_can_set_password_field_using_environment(self):
        config = Configuration.load([], merge({'DAST_PASSWORD_FIELD': 'password'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_password_field, 'password')

    def test_can_set_password_field_using_legacy_environment(self):
        config = Configuration.load([], merge({'AUTH_PASSWORD_FIELD': 'password'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_password_field, 'password')

    def test_can_set_auth_submit_field_using_option(self):
        config = Configuration.load(['--auth-submit-field', 'field'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_submit_field, 'field')

    def test_can_set_auth_submit_field_using_environment(self):
        config = Configuration.load([], merge({'DAST_SUBMIT_FIELD': 'field'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_submit_field, 'field')

    def test_can_set_auth_submit_field_using_legacy_environment(self):
        config = Configuration.load([], merge({'AUTH_SUBMIT_FIELD': 'field'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_submit_field, 'field')

    def test_can_set_auth_first_submit_field_using_option(self):
        config = Configuration.load(['--auth-first-submit-field', 'field'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_first_submit_field, 'field')

    def test_can_set_auth_first_submit_field_using_environment(self):
        config = Configuration.load([], merge({'DAST_FIRST_SUBMIT_FIELD': 'field'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_first_submit_field, 'field')

    def test_can_set_auth_first_submit_field_using_legacy_environment(self):
        config = Configuration.load([], merge({'AUTH_FIRST_SUBMIT_FIELD': 'field'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_first_submit_field, 'field')

    def test_can_set_auth_display_using_option(self):
        config = Configuration.load(['--auth-display', 'True'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_display, True)

    def test_can_set_auth_display_using_environment(self):
        config = Configuration.load([], merge({'DAST_AUTH_DISPLAY': 'True'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_display, True)

    def test_can_set_auth_display_using_legacy_environment(self):
        config = Configuration.load([], merge({'AUTH_DISPLAY': 'True'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_display, True)

    def test_can_set_auth_auto_using_option(self):
        config = Configuration.load(['--auth-auto', 'true'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_auto, True)

    def test_can_set_auth_auto_using_environment(self):
        config = Configuration.load([], merge({'DAST_AUTH_AUTO': 'true'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_auto, True)

    def test_can_set_auth_auto_using_legacy_environment(self):
        config = Configuration.load([], merge({'AUTH_AUTO': 'true'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_auto, True)

    def test_can_not_set_auth_exclude_urls_to_have_any_invalid_url(self):
        with self.assertRaises(SystemExit) as system_exit, utilities.capture_output() as output:
            Configuration.load(['--auth-exclude-urls', 'http://website1 , http://website2   , invalidurl'],
                               self.DEFAULT_ENV)

        self.assertEqual(system_exit.exception.code, 2)
        self.assertIn('argument --auth-exclude-urls: invalidurl is not a valid URL', output.stderr.getvalue())

    def test_can_set_auth_exclude_urls_using_option(self):
        config = Configuration.load(['--auth-exclude-urls', 'http://website1 , http://website2  '], self.DEFAULT_ENV)
        self.assertEqual(config.auth_exclude_urls, ['http://website1', 'http://website2'])

    def test_can_set_auth_exclude_urls_using_environment(self):
        environment = merge({'DAST_AUTH_EXCLUDE_URLS': 'http://website1,http://website2'}, self.DEFAULT_ENV)
        config = Configuration.load([], environment)
        self.assertEqual(config.auth_exclude_urls, ['http://website1', 'http://website2'])

    def test_can_set_auth_exclude_urls_using_legacy_environment(self):
        environment = merge({'AUTH_EXCLUDE_URLS': 'http://website1,http://website2'}, self.DEFAULT_ENV)
        config = Configuration.load([], environment)
        self.assertEqual(config.auth_exclude_urls, ['http://website1', 'http://website2'])

    def test_enable_auto_update_addons_using_option(self):
        config = Configuration.load(["--auto-update-addons", "True"], self.DEFAULT_ENV)
        self.assertEqual(config.auto_update_addons, True)

    def test_can_set_exclude_rules_using_option(self):
        config = Configuration.load(['--exclude-rules', ' 10001 , 20002  '], self.DEFAULT_ENV)
        self.assertEqual(config.exclude_rules, ['10001', '20002'])

    def test_can_set_exclude_rules_using_environment(self):
        environment = merge({'DAST_EXCLUDE_RULES': '56,57'}, self.DEFAULT_ENV)
        config = Configuration.load([], environment)
        self.assertEqual(config.exclude_rules, ['56', '57'])

    def test_no_rules_are_excluded_by_default(self):
        config = Configuration.load([], self.DEFAULT_ENV)
        self.assertEqual(config.exclude_rules, [])

    def test_can_set_request_headers_using_option(self):
        config = Configuration.load(['--request-headers', ' Authorization: Bearer 123456789 , Accept: */*  '],
                                    self.DEFAULT_ENV)
        self.assertDictEqual(config.request_headers, {'Authorization': 'Bearer 123456789', 'Accept': '*/*'})

    def test_can_set_request_headers_using_environment(self):
        environment = merge({'DAST_REQUEST_HEADERS': 'Cache-control: no-cache'}, self.DEFAULT_ENV)
        config = Configuration.load([], environment)
        self.assertDictEqual(config.request_headers, {'Cache-control': 'no-cache'})

    def test_setting_request_headers_aborts_when_format_invalid(self):
        with self.assertRaises(SystemExit) as system_exit, utilities.capture_output() as output:
            Configuration.load(['--request-headers', 'Too:many:colons'], {})

        self.assertEqual(system_exit.exception.code, 2)
        self.assertIn('Failed to parse request headers, aborting', output.stderr.getvalue())

    def test_no_request_headers_are_set_by_default(self):
        config = Configuration.load([], self.DEFAULT_ENV)
        self.assertDictEqual(config.request_headers, {})

    def test_disable_auto_update_addons_using_option(self):
        config = Configuration.load(["--auto-update-addons", "False"], self.DEFAULT_ENV)
        self.assertEqual(config.auto_update_addons, False)

    def test_auto_update_addons_is_true_by_default(self):
        config = Configuration.load([], self.DEFAULT_ENV)
        self.assertEqual(config.auto_update_addons, True)

    def test_disable_auto_update_addons_using_environment(self):
        config = Configuration.load([], merge({'DAST_AUTO_UPDATE_ADDONS': '0'}, self.DEFAULT_ENV))
        self.assertEqual(config.auto_update_addons, False)

    def test_can_set_full_scan_using_option_passing_true(self):
        config = Configuration.load(["--full-scan", "True"], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan, True)

    def test_can_set_full_scan_using_option_passing_1(self):
        config = Configuration.load(["--full-scan", "1"], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan, True)

    def test_can_set_full_scan_using_option_passing_false(self):
        config = Configuration.load(["--full-scan", "false"], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan, False)

    def test_can_set_full_scan_using_option_passing_0(self):
        config = Configuration.load(["--full-scan", "0"], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan, False)

    def test_can_set_full_scan_using_option(self):
        config = Configuration.load(["--full-scan", "true"], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan, True)

    def test_can_set_full_scan_should_by_falsy_when_junk_is_passed_in(self):
        config = Configuration.load(["--full-scan", "not-a-boolean-value"], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan, False)

    def test_can_set_full_scan_using_environment(self):
        environment = merge({'DAST_FULL_SCAN_ENABLED': 'true'}, self.DEFAULT_ENV)
        config = Configuration.load([], environment)
        self.assertEqual(config.full_scan, True)

    def test_can_set_validate_domain_using_option_passing_true(self):
        config = Configuration.load(["--validate-domain", "True"], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan_domain_validation_required, True)

    def test_can_set_validate_domain_using_option_passing_1(self):
        config = Configuration.load(["--validate-domain", "1"], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan_domain_validation_required, True)

    def test_can_set_validate_domain_using_option_passing_false(self):
        config = Configuration.load(["--validate-domain", "false"], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan_domain_validation_required, False)

    def test_can_set_validate_domain_using_option_passing_0(self):
        config = Configuration.load(["--validate-domain", "0"], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan_domain_validation_required, False)

    def test_can_set_domain_validation_using_option(self):
        config = Configuration.load(["--validate-domain", "true"], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan_domain_validation_required, True)

    def test_can_set_full_scan_domain_validation_required_using_environment(self):
        environment = merge({'DAST_FULL_SCAN_DOMAIN_VALIDATION_REQUIRED': 'true'}, self.DEFAULT_ENV)
        config = Configuration.load([], environment)
        self.assertEqual(config.full_scan_domain_validation_required, True)

    def test_can_set_availability_timeout_using_option(self):
        config = Configuration.load(["--availability-timeout", "10"], self.DEFAULT_ENV)
        self.assertEqual(config.availability_timeout, 10)

    def test_can_set_availability_timeout_using_environment(self):
        environment = merge({'DAST_TARGET_AVAILABILITY_TIMEOUT': '20'}, self.DEFAULT_ENV)
        config = Configuration.load([], environment)
        self.assertEqual(config.availability_timeout, 20)

    def test_can_set_zap_option_config_file(self):
        config = Configuration.load(['-c', 'config.file'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_config_file, 'config.file')

    def test_can_set_zap_option_config_url(self):
        config = Configuration.load(['-u', 'http://config.url'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_config_url, 'http://config.url')

    def test_can_set_zap_option_gen_file(self):
        config = Configuration.load(['-g', 'gen_file'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_gen_file, 'gen_file')

    def test_can_set_zap_option_mins(self):
        config = Configuration.load(['-m', 'mins'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_mins, 'mins')

    def test_can_set_zap_option_report_html(self):
        config = Configuration.load(['-r', 'report_html'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_report_html, 'report_html')

    def test_can_set_zap_option_report_md(self):
        config = Configuration.load(['-w', 'report_md'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_report_md, 'report_md')

    def test_can_set_zap_option_report_xml(self):
        config = Configuration.load(['-x', 'report_xml'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_report_xml, 'report_xml')

    def test_can_set_zap_option_report_include_alpha_rules(self):
        config = Configuration.load(['-a'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_include_alpha, True)

    def test_can_set_zap_option_report_show_debug_msgs(self):
        config = Configuration.load(['-d'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_debug, True)

    def test_can_set_zap_option_port(self):
        config = Configuration.load(['-P', '2001'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_port, '2001')

    def test_can_set_zap_option_delay_in_seconds(self):
        config = Configuration.load(['-D', '12'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_delay_in_seconds, '12')

    def test_can_set_zap_option_default_info(self):
        config = Configuration.load(['-i'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_default_info, True)

    def test_can_set_zap_option_no_fail_on_warn(self):
        config = Configuration.load(['-I'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_no_fail_on_warn, True)

    def test_can_set_zap_option_use_ajax_spider(self):
        config = Configuration.load(['-j'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_use_ajax_spider, True)

    def test_can_set_zap_option_min_level(self):
        config = Configuration.load(['-l', 'INFO'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_min_level, 'INFO')

    def test_can_set_zap_option_context(self):
        config = Configuration.load(['-n', 'context.file'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_context_file, 'context.file')

    def test_can_set_zap_option_progress(self):
        config = Configuration.load(['-p', 'progress.file'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_progress_file, 'progress.file')

    def test_can_set_zap_option_short_format(self):
        config = Configuration.load(['-s'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_short_format, True)

    def test_can_set_zap_option_mins_to_wait(self):
        config = Configuration.load(['-T', '1'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_mins_to_wait, '1')

    def test_can_set_zap_option_other_options(self):
        config = Configuration.load(['-z', 'funtimes=3'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_other_options, 'funtimes=3')
