#!/usr/bin/env python
import sys
import json

if len(sys.argv) != 2:
    print(f'Usage: ./normalize_dast_report.py <path-to-json>')
    exit(1)

with open(sys.argv[1]) as json_file:
    report = json.loads(json_file.read())
    del report['@generated']
    del report['@version']
    for vulnerability in report['vulnerabilities']:
        if('id' in vulnerability):
            del vulnerability['id']
    sys.stdout.write(json.dumps(report, indent=2, sort_keys=True))
