#!/bin/sh
# Run all unit tests:   ./test/test-unit.sh
# Run a specific test:  ./test/test-unit.sh -p test_configur*.py
# More information:     ./test/test-unit.sh --help


TEST_DIR="$( cd "$(dirname "$0")" || exit ; pwd -P )"

PYTHONPATH="$TEST_DIR/../src" python3 -m unittest discover -v -s "$TEST_DIR/unit" "$@"
